#pragma once

#include "Application.hh"
#include "Loader.hh"

class RunApp : public Application {
SIMPLE_APPLICATION(RunApp, "running-example")
public: 
	void init(Loader *, const Config &) override;
};