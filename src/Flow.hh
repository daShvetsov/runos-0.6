#pragma once

#include "FlowFwd.hh"

#include <cstdint>
#include <utility> // pair
#include <unordered_set>

#include "types/exception.hh"

namespace runos {

class Flow {
public:
    enum class State {
        Egg,
        Active,
        Evicted,
        Idle,
        Expired,
    };

    Flow() noexcept;

    virtual State state() const
    { return m_state; }

    virtual uint64_t cookie() const
    { return m_cookie; }

    virtual void cookie(uint64_t cookie)
    { m_cookie = cookie; }

    // Returns (cookie_base, cookie_mask)
    static std::pair<uint64_t, uint64_t> cookie_space();

    virtual void evict() { } // = 0; //TODO
    virtual void kill() { } // = 0; //TODO

protected:
    State m_state {State::Egg};
    uint64_t m_cookie;
};

} // namespace runos
