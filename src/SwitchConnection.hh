#pragma once

#include "SwitchConnectionFwd.hh"

#include <cstdint>
#include <cstddef>

#include <QMetaType>

namespace fluid_base {
class OFConnection;
}

namespace fluid_msg {
class OFMsg;
}

namespace runos {

class SwitchConnection {
    const uint64_t m_dpid;

public:
    uint64_t dpid() const
    { return m_dpid; }

    bool alive() const;

    uint8_t version() const;

    void send(const fluid_msg::OFMsg& msg);

	void send(uint8_t *buf, uint16_t length);
    
    void close();

protected:
    fluid_base::OFConnection* m_ofconn;
    SwitchConnection(fluid_base::OFConnection* ofconn, uint64_t dpid);
};

} // namespace runos

Q_DECLARE_METATYPE(runos::SwitchConnectionPtr);
