#include "RunApp.hh"

#include <mutex>
#include <unordered_map>
#include <map>
#include <iterator>
#include "api/Packet.hh"
#include "api/PacketMissHandler.hh"
#include "api/TraceablePacket.hh"
#include "types/ethaddr.hh"
#include "oxm/openflow_basic.hh"

#include "Controller.hh"
#include "Topology.hh"
#include "SwitchConnection.hh"
#include "Flow.hh"

REGISTER_APPLICATION(RunApp, {"controller", ""})

using namespace runos;

class SwitchBase { 
	std::mutex mutex;
	typedef std::unordered_map<ethaddr, uint32_t> switchDataBase;
	std::map<uint64_t, switchDataBase> db;
public : 
	void learn(uint64_t dpid, uint32_t in_port, ethaddr mac)
	{
		LOG(INFO) << "learn on switch : " << dpid << " ethernet addr " 
		<< mac << " on port " << in_port;
		if (is_broadcast(mac)) { 
			DLOG(WARNING) << "broadcast source address";
			return ;
		}
        LOG(INFO) << mac << " seen at " << dpid << ':' << in_port;
        {
            mutex.lock();
            db[dpid].emplace(mac, in_port);
            //db.emplace(mac, switch_and_port{dpid, in_port});
            mutex.unlock();
        }
	}
	int64_t query(ethaddr mac, uint32_t dpid)
    {
    	LOG(INFO) << "Serching " << mac << " in " << dpid;
        mutex.lock();
        int64_t ret;
        auto it = db[dpid].find(mac);
        if (it != db[dpid].end()){
        	LOG(INFO) << "FOUND!";
            ret = it->second;
        }
        else{
        	LOG(INFO) << "Not found";
            ret = -1;
        }
        mutex.unlock();
        return ret;
    }

};

void RunApp::init(Loader *loader, const Config &){
	Controller *ctrl = Controller::get(loader);
	auto db = std::make_shared<SwitchBase>();
	ctrl->registerHandler("forwarding",
	[=](SwitchConnectionPtr connection) {
		return [=](Packet& pkt, FlowPtr flow, Decision decision) {
			ethaddr dst_mac = pkt.load(oxm::eth_dst());
			db->learn(connection->dpid(), 
						pkt.load(oxm::in_port()),
						pkt.load(oxm::eth_src()));

			int64_t target64 = db->query(dst_mac, connection->dpid());
			if (target64 != -1){
				uint32_t target = (uint32_t)target64;
				return decision.unicast(target)
						.idle_timeout(std::chrono::seconds(60))
						.hard_timeout(std::chrono::seconds(300));
			} else {
				if (not is_broadcast(dst_mac)) { 
					LOG(INFO) << "broadcast unknow address";
					return decision.broadcast()
                        .idle_timeout(std::chrono::seconds::zero());
				}
				return decision.broadcast();
			}
		};
	});
}